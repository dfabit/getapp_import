﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;


namespace getapp_import
{
    class Program
    {
        static void Main(string[] args)
        {
            var providerNameParam = args[0];
            var fileParam = args[1];

            // Get provider file
            string Document = System.IO.File.ReadAllText(fileParam); 

            // Setup the input
            var input = new StringReader(Document);

            // Get provider name parameter
            ProviderMapper providerMapper = new ProviderMapper(providerNameParam);

            var provider = providerMapper.MapDocument(input);

            if (provider.Products != null)
            {
                var connectionString = "Data Source = MSSQL1; Initial Catalog = ProductsDB;Integrated Security=true;";
                SaasRepository saasRepository = new SaasRepository(connectionString);

                foreach (var item in provider.Products)
                {
                    Console.WriteLine("importing: Name: {0} Categories: {1} Twitter: {2}", item.Name, item.Tags, item.Twitter);
                    // Add product to the fake repository
                    saasRepository.AddProduct(item);
                }
            }

        }

    }
}
