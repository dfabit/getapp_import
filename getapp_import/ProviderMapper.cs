﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;

namespace getapp_import
{
    public class ProviderMapper
    {
        string ProviderName { get; set; }

        public ProviderMapper(string providerName)
        {
            ProviderName = providerName;
        }

        public Provider MapDocument(StringReader document)
        {
            switch (ProviderName)
            {
                case "capterra":
                    return MapCapterra(document);
                case "softwareadvice":
                    throw new NotImplementedException();
                default:
                    Console.WriteLine("Not supported provider");
                    return new Provider();
            }

        }

        private Provider MapCapterra(StringReader document)
        {
            var deserializer = new DeserializerBuilder()
                .WithNamingConvention(new CamelCaseNamingConvention())
                .Build();

            return deserializer.Deserialize<Provider>(document);
        }

    }
}
